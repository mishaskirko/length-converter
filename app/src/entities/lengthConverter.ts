import {IConverter} from '../interfaces/IConverter';
import * as fs from 'fs';

export class LengthConverter implements IConverter {

  private readonly conversionRules: {
    [key: string]: {
      [key: string]: number;
    };
  };

  constructor(private units: string[], private readonly conversionRulesPath: string) {
    this.units = units;
    this.conversionRulesPath = conversionRulesPath;
    this.conversionRules = JSON.parse(fs.readFileSync(this.conversionRulesPath, 'utf8'));
  }

  convert(distance: number, from: string, to: string): JSON {
    if (this.units.indexOf(from) === -1 || this.units.indexOf(to) === -1) {
      throw new Error('Invalid unit');
    }
    if (from === to) {
      return JSON.parse(`{"${from}": ${distance}}`);
    }
    const result = distance * this.conversionRules[from][to];
    return JSON.parse(`{"unit": "${to}", "value": ${result}}`);
  }

}