import * as http from 'http';
import {LengthConverter} from './entities/lengthConverter';

const server = http.createServer((request, response) => {
  if (request.url === '/') {
    const converter = new LengthConverter(['m', 'cm', 'ft', 'in', 'km', 'mm', 'yd'], './src/config/conversionRules.json');
    const testValuesJson = {"distance": {"unit": "m", "value": 1}, "convert_to": "yd"};
    const result = JSON.stringify(
      converter.convert(
        testValuesJson.distance.value,
        testValuesJson.distance.unit,
        testValuesJson.convert_to
      )
    );
    response.end(result);
  }
});

server.listen(3000, () => {
  console.log('Server listening on port 3000');
});
