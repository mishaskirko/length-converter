export interface IConverter {
  convert(value: number, fromUnit: string, toUnit: string): JSON;
}